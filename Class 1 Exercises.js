/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)

let surfaceAreaThirteen;
let surfaceAreaSeventeen;
const diam = 13;
const diamTwo = 17;
let pricePerSquareInchThirteen;
let pricePerSquareInchSeventeen;
const cost = 16.99;
const costTwo = 19.99;
let radius = (diam / 2);
let radiusTwo = (diamTwo / 2);
let power = Math.pow(radius, 2);
let powerTwo = Math.pow(radiusTwo, 2);

surfaceAreaThirteen = Math.PI * power;
console.log("The area for the 13\" pizza is: " + surfaceAreaThirteen + " inches");
surfaceAreaSeventeen = Math.PI * powerTwo;
console.log("The area for the 17\" pizza is: " + surfaceAreaSeventeen + " inches");


// 2. What is the cost per square inch of each pizza?
pricePerSquareInchThirteen = (surfaceAreaThirteen / cost);
pricePerSquareInchSeventeen = (surfaceAreaSeventeen / costTwo);
console.log("The cost per square inch for the 13\" pizza is: $" + pricePerSquareInchThirteen);
console.log("The cost per square inch for the 17\" pizza is: $" + pricePerSquareInchSeventeen);

// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)
const randomCard = Math.floor(Math.random() * 13) + 1;
Math.random();

// 4. Draw 3 cards and use Math to determine the highest
// card
let compare = [1, 2, 3];
let big;
for (let i = 0; i < 3; i++) {
  compare[i] = Math.floor(Math.random() * 13) + 1;
  if (compare[i] >= big) {
    big = compare[i];
  }
  big = Math.max(compare[i]);

}
console.log("The highest card is: " + big);

/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.
let firstName = 'Harry'
let lastName = 'Potter'
let streetAddress = '321 Diagon Alley'
let city = 'London'
let state = 'England'
let zipCode = '23536'
const newLine = "\n"

const format = `${firstName} ${lastName}
${streetAddress}
${city} ${state}
${zipCode}`;

// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
//
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring
const linez = (format).split('\n');
const stringz = (format).split(linez);
for (var i = 0; i < stringz.length; i++) {
  console.log(stringz[i]);
}

/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00
//
// Look online for documentation on Date objects.

const startDate = new Date(2020, 0, 1);
const endDate = new Date(2020, 3, 1);
var middleDate = new Date((startDate.getTime() + endDate.getTime()) / 2);
console.log(middleDate);

// Starting hint:
//const endDate = new Date(2019, 3, 1);